package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest(showSql = false)
@DirtiesContext
public class PersonTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    EntityManager em;

    @Test
    void name() {
        assertTrue(true);
    }

    @Test
    void test_2() {
        final Person p = new Person(1L, "bin", "wang");
        assertFalse(personRepository.findById(1L).isPresent());
    }

    @Test
    void test_3() {
        Person person = new Person(1L, "wang", "wang");
        personRepository.save(person);

        em.flush();
        em.clear();
        Optional<Person> p = personRepository.findById(1L);


        assertThat(p.isPresent()).isTrue();
        assertEquals(p.get().getFirstName(), "wang");
    }
}
